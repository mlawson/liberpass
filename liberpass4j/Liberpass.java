package liberpass4j;
/**
 * @author Mac Lawson
 * @version 1.0
 * liberpass implementation class for liberpass 
 * part of the liberpass4j lib
 * Git: https://0xacab.org/mlawson/liberpass.git
 */
public class Liberpass {
	public static String username;
	public static String email;
	public static String password;

	public Liberpass(String username, String email, String password) {

		username = username;
		email = email;
		password = password;

	}

	public String getPassword() {

		return password;
	}

	public static void setPassword(String newPassword) {
		password = newPassword;
		
	}

	public String getUsername() {

		return username;

	}

	public static void setUsername(String newUsername) {

		username = newUsername;

	}

	public String getEmail() {

		return email;

	}

	public static void setEmail(String newEmail) {

		email = newEmail;
	}

}
